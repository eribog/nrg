var growattDayGraph=function() {
    function graph(container, title) {
        var data=[]
        $.ajax({
            url: "getJSON.php?data=lastGrowatt",
            async: false,
            dataType: "json",
            success: function(json) {
                data=json;
            }
        });
        return new Highcharts.Chart({
            chart: {
                type: 'spline',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                min: 0
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: data
        });
    }

    function update(graph, fields, data) {
        var graph1 = graph.series[0];
        var time=new Date(data.datetime.replace(" ", "T")).getTime() - (new Date().getTimezoneOffset() * 60000);

        graph1.addPoint([time,parseFloat(data[fields[0]])*1000]);

        var first=graph1.points[0];
        if(first.x < (time - (60 * 60 * 3600 * 2))) {
            first.remove();
        }
    }

    return {
        graph:graph,
        update:update
    }
}();
