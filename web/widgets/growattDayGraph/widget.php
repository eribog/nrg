<?php
class growattDayGraph extends widget implements widgetInterface {
    const NAME="growattDayGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "https://code.jquery.com/jquery-1.9.1.js",
                "https://code.highcharts.com/highcharts.js",
                "https://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "field",
        "max",
        "title"
    );

    public function getUpdate() {
        $field=$this->getParam("field");
        return array("current" => "growattDayGraph.update(" . $this->getName() .
            ", '" . $field . "', data)");
    }

}
