var meterGrowatt=function() {
    function update(data) {
        document.getElementById("EacToday").innerHTML=parseFloat(data.EacToday).toFixed(1);
        document.getElementById("EacTotal").innerHTML=parseFloat(data.EacTotal).toFixed(1);
    }
    
    return {
        update:update
    };
}();

