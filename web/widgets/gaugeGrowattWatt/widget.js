var gaugeGrowattWatt=function() {

    function gauge(container, title, max) {
        var gaugeTitle = "Watt";
        return new Highcharts.Chart({
            chart: {
                renderTo: container,
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false 
            },

            title: {
                text: title
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#222']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: max,

                minorTickInterval: max/50,

                tickInterval: max/8,
                tickWidth: 3,
                tickPosition: 'inside',
                tickLength: 12,
                tickColor: '#444',
                labels: {
                    step: 1,
                    rotation: 'auto'
                },
                title: {
                    text: gaugeTitle
                },
                plotBands: [{
                    from: 0,
                    to: max/10,
                    color: {
                        linearGradient: { x1: 1, x2: 0, y1: 0, y2: 0 },
                        stops: [
                            [0, '#000000'],
                            [1, '#33993b']
                        ]
                    }
                }, {
                    from: max/10,
                    to: max/2,
                    color: {
                        linearGradient: { x1: 0, x2: 0, y1: 1, y2: 0 },
                        stops: [
                            [0, '#33993b'],
                            [1, '#55bf3b']
                        ]
                    }
                }, {
                    from: max/2,
                    to: max,
                    color: {
                        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                        stops: [
                            [0, '#55bf3b'],
                            [1, '#00ff00']
                        ]
                    }

                }]
            },

            series: [{
                name: 'Current Output',
                data: [0],
                tooltip: {
                    valueSuffix: 'Watt'
                }, 
                yAxis: 0
            }]
        });
    }

    function update(widget, field, data) {
        var pointTotal = widget.series[0].points[0];
        pointTotal.update(parseFloat(data[field]));
    }

    return {
        gauge:gauge,
        update:update
    };
}();
