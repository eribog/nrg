<?php

error_reporting(E_ALL); ini_set('display_errors', 1);
set_include_path("..");
require_once("include/include.php");


$data=$_GET["data"];

if ($data=="current") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\memory());

    $measurement=$measurement->getLast();

    echo json_encode($measurement);
} else if ($data=="lasthour") {
    $measurement=new collector\dsmr\measurement(new collector\dsmr\repository\memory());

    $measurement=$measurement->getLastHour();

    echo json_encode($measurement);

} else if ($data=="currentGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\all());

    $measurement=$measurement->getLast();

    echo json_encode($measurement);
} else if ($data=="lastGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\all());

    $measurement=$measurement->getLastDay();

    echo json_encode($measurement);


} else if ($data=="monthGrowatt") {
    $measurement=new collector\growatt\measurement(new collector\growatt\repository\all());

    $measurement=$measurement->getMonthEacToday();

    echo json_encode($measurement);


} else {
    echo "No data reqested";
}

?>
