<?php
/**
 * Autoload classes
 */
function autoload($file) {
    if (get_include_path()=="..") {
        $file="../" . $file;
    }
    if (is_readable($file)) {
        require_once $file;
    } else {
        return false;
    }

}

function autoloadClass($class) {
    $file="classes/" . str_replace("\\", "/", $class) . ".php";
    return autoload($file);
}

function autoloadInterface($interface) {
    $file="interfaces/" . $interface . ".php";
    return autoload($file);
}

spl_autoload_register("autoloadClass");
spl_autoload_register("autoloadInterface");

?>
