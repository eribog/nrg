<?php
namespace collector\growatt;

use DateTime;

class telegram {
    private $buffer;
    private $data=array();

    public function __construct($buffer, $proto = 2, $software="1.0.0.0") {
        $this->buffer=$buffer;
        $header = unpack("C8pre/a10serial/a10ident", substr($buffer, 0, 28));

        $this->data["serial"]=$header["serial"];
        $this->data["ident"]=$header["ident"];
        $this->data["datetime"]=new DateTime();

        // remove header + 5/11bytes
        if (substr($software,1) < 2) {
            $energy = substr($buffer, 28 + 11);
        } else {
            // not tested
            $energy = substr($buffer, 28 + 5);
        }

        foreach ($this->getDataList() as $name => $value) {
            $this->data[$name] = $value->getFromBuffer($energy);
            $energy = $value->getRemaining();
        }

    }

    public function getData() {
        return $this->data;
    }

    public function getDataList() {
        return array(
            "InvStat"   => new value("InvStat", "", 2, 0,"Status"),
            "Ppv"       => new value("Ppv", "W", 4, 1),
            "Vpv1"      => new value("Vpv1", "V", 2, 1),
            "Ipv1"      => new value("Ipv1", "A", 2, 1),
            "Ppv1"      => new value("Ppv1", "W", 4, 1),
            "Vpv2"      => new value("Vpv2", "V", 2, 1),
            "Ipv2"      => new value("Ipv2", "A", 2, 1),
            "Ppv2"      => new value("Ppv2", "W", 4, 1),
            "Pac"       => new value("Pac", "W", 4, 1),
            "Fac"       => new value("Fac", "Hz", 2, 2),
            "Vac1"      => new value("VacR", "V", 2, 1),
            "Iac1"      => new value("IacR", "A", 2, 1),
            "Pac1"      => new value("PacR", "W", 4, 1),
            "Vac2"      => new value("VacS", "V", 2, 1),
            "Iac2"      => new value("IacS", "A", 2, 1),
            "Pac2"      => new value("PacS", "W", 4, 1),
            "Vac3"      => new value("VacT", "V", 2, 1),
            "Iac3"      => new value("IacT", "A", 2, 1),
            "Pac3"      => new value("PacT", "W", 4, 1),
            "EacToday"  => new value("EacToday", "kWh", 4, 1),
            "EacTotal"  => new value("EacTotal", "kWh", 4, 1),
            "Tall"      => new value("Total", "s", 4, 1),
            "Temp"       => new value("Temp", "&deg;C", 2, 1),
            "ISOFault"  => new value("ISOFault" , "V", 2, 1),
            "GFCIFault" => new value("GFCIFault" , "mA", 2, 1),
            "VpvFault"  => new value("VpvFault" , "V", 2, 1),
            "VacFault"  => new value("VacFault" , "V", 2, 1),
            "FacFault"  => new value("DCIFault" , "Hz", 2, 1),
            "TempFault" => new value("TempFault", "&deg;C", 2, 1),
            "Unknown0"  => new value("Unknown0", "", 2, 0),
            "Faultcode" => new value("Faultcode" , "", 2, 0),
            "IPMTemp"   => new value("IPMtemp", "&deg;C", 2, 1),
            "Pbusvolt"  => new value("Pbusvolt", "V", 2, 1),
            "Nbusvolt"  => new value("Nbusvolt", "V", 2, 1),
            "Unknown1"  => new value("Unknown1", "", 4, 0),
            "Unknown2"  => new value("Unknown2", "", 4, 0),
            "Unknown3"  => new value("Unknown3", "", 4, 0),
            "Epv1Today" => new value("Epv1today", "kWh", 4, 1),
            "Epv1Total" => new value("Epv1total", "kWh", 4, 1),
            "Epv2Today" => new value("Epv2today", "kWh", 4, 1),
            "Epv2Total" => new value("Epv2total", "kWh", 4, 1),
            "EpvTotal"  => new value("Epvtotal", "kWh", 4, 1),
            "Rac"       => new value("Rac", "Var", 4, 1),
            "ERactoday" => new value("RacToday", "Kvarh", 4, 1),
            "ERactotal" => new value("RacTotal", "Kvarh", 4, 1)
        );
    }
}



?>
