<?php
namespace collector\dsmr\obis;

use Exception;

class obis {
    private $line;

    private $name;
    private $data;

    private static $register=array();

    public function __construct($line) {
        if (empty(self::$register)) {
            $this->registerObis();
        }
        $this->line=$line;
        $this->identify();
    }

    public function isValid() {
        return true;
    }

    public function getName() {
        return $this->name;
    }

    public function getData() {
        return $this->data;
    }

    private function identify() {
        $id_regex="/([[:xdigit:]]-[[:xdigit:]]:[[:xdigit:]]{1,3}\.[[:xdigit:]]{1,3}\.[[:xdigit:]]{1,3})(.+)/";
        $id=preg_replace($id_regex, "\\1", $this->line);
        $data=preg_replace($id_regex, "\\2", $this->line);
        $value=$this->getFromId($id);
        $value->setValue($data);
        $this->data=$value;
        $this->name=$value->getName();
    }

    private function registerObis() {
        $obis=array(
            new obisValue("1-3:0.2.8",  "version",  new obisString(2,2),    "P1 Version"),
            new obisValue("0-0:1.0.0",  "datetime", new obisTST(),          "Date / Time"),
            new obisValue("0-0:96.1.1", "eid",      new obisString(0,96),   "Equipment identifier"),
            new obisValue("1-0:1.8.1",  "T1",       new obisFloat(9, 3, 3), "Delivered Tariff 1"),
            new obisValue("1-0:2.8.1",  "T1R",      new obisFloat(9, 3, 3), "Returned Tariff 1"),
            new obisValue("1-0:1.8.2",  "T2",       new obisFloat(9, 3, 3), "Delivered Tariff 2"),
            new obisValue("1-0:2.8.2",  "T2R",      new obisFloat(9, 3, 3), "Returned Tariff 2"),
            new obisValue("0-0:96.14.0","tariff",   new obisString(4, 4),   "Current Tariff"),
            new obisValue("1-0:1.7.0",  "current",  new obisFloat(5, 3 ,3), "Current Delivery"),
            new obisValue("1-0:2.7.0",  "currentR", new obisFloat(5, 3 ,3), "Current Return"),
            new obisValue("0-0:17.0.0", "Threshold",new obisFloat(4, 1, 1), "Current Threshold"),
            new obisValue("0-0:96.3.10","switchE",  new obisBool(),         "ElectricitySwitch"),
            new obisValue("0-0:96.7.21","PowFail",  new obisFloat(5),       "Number of Power Failures"),
            new obisValue("0-0:96.7.9", "PowFailLong",new obisFloat(5),     "Number of Long Power Failures"),
            new obisValue("1-0:99.97.0","PowFailAllLongLog", new obisBuffer, "Power Failures"),
            new obisValue("0-0:96.7.19","PowFailLogEntry", new obisMulti(array(new obisTST(), new obisFloat(10,0,0))), "Power Failures"),
            new obisValue("1-0:31.7.0", "CurrentAmpL1",new obisFloat(3),    "Current usage in A phase 1"),
            new obisValue("1-0:51.7.0", "CurrentAmpL2",new obisFloat(3),    "Current usage in A phase 2"),
            new obisValue("1-0:71.7.0", "CurrentAmpL3",new obisFloat(3),    "Current usage in A phase 3"),
            new obisValue("1-0:21.7.0", "CurrentL1",new obisFloat(2,3,3),   "Current usage in kW phase 1"),
            new obisValue("1-0:41.7.0", "CurrentL2",new obisFloat(2,3,3),   "Current usage in kW phase 2"),
            new obisValue("1-0:61.7.0", "CurrentL3",new obisFloat(2,3,3),   "Current usage in kW phase 3"),
            new obisValue("1-0:22.7.0", "CurrentRL1",new obisFloat(2,3,3),  "Current return in kW phase 1"),
            new obisValue("1-0:42.7.0", "CurrentRL2",new obisFloat(2,3,3),  "Current return in kW phase 2"),
            new obisValue("1-0:62.7.0", "CurrentRL3",new obisFloat(2,3,3),  "Current return in kW phase 3"),
            new obisValue("1-0:32.32.0","SagsL1",   new obisFloat(5),       "Number of voltage sags in phase 1"),
            new obisValue("1-0:52.32.0","SagsL2",   new obisFloat(5),       "Number of voltage sags in phase 2"),
            new obisValue("1-0:72.32.0","SagsL3",   new obisFloat(5),       "Number of voltage sags in phase 3"),
            new obisValue("1-0:32.36.0","SwellsL1", new obisFloat(5),       "Number of voltage swells in phase 1"),
            new obisValue("1-0:52.36.0","SwellsL2", new obisFloat(5),       "Number of voltage swells in phase 2"),
            new obisValue("1-0:72.36.0","SwellsL3", new obisFloat(5),       "Number of voltage swells in phase 3"),
            new obisValue("0-0:96.13.1","MsgCode",  new obisString(0,8),    "Message Code"),
            new obisValue("0-0:96.13.0","MsgText",  new obisString(0,2048), "Message Text"),
            //Mbus 1, Mbus 2-4 currently not implemented
            // Devices other than gas meter, currently not implemented
            new obisValue("0-1:24.1.0", "DevTypeM1",new obisFloat(3),       "Device Type Mbus 1"),
            new obisValue("0-1:96.1.0", "EidM1",    new obisString(0,96),   "Equipment Identifier Mbus 1"),
            new obisValue("0-1:24.2.1", "GasM1",    new obisMulti(array(new obisTST(), new obisFloat(8,2,3))), "Gas Delivery Mbus 1"),
            new obisValue("0-1:24.4.0", "GasValveM1",new obisBool(),        "Gas Valve Mbus 1")
        );

        foreach ($obis as $value) {
            self::$register[$value->getId()]=$value;
        }
    }

    private function getFromId($id) {
        if (isset(self::$register[$id])) {
            return self::$register[$id];
        } else {
            throw new Exception("Unknown ID: " . $id);
        }

    }

}
