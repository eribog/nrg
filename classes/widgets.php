<?php

class widgets {
    private static $widgets=array();

    public static function initialize() {
        foreach (glob("./widgets/*", GLOB_ONLYDIR) as $dir) {
            $class=basename($dir);
            if (file_exists($dir . "/widget.php")) {
                require_once($dir . "/widget.php");
                static::$widgets[]=$class;
            }

         }

    }
}
